
#include "gc_impl.hpp"

#include <type_traits>
#include <assert.h>

#define countOf(array)  (sizeof(array)/sizeof(array[0]))

using namespace GC;

struct PtrAndType
{
    const void *ptr;
    TypeInfo *type;
};

template<class T>
struct GC_Traits {
    static TypeInfo* typeInfo() {
        return T::typeInfo();
    }
};

template<class T>
struct GC_Traits<T *> {
    static TypeInfo* typeInfo() {
        return TypeInfo::pointerInfo();
    }
};

template<class T> inline OffsetInfo fld(const T& field) {
    OffsetInfo info = {(int)&field, GC_Traits<T>::typeInfo()};
    return info;
}

template<class T, class Base> inline OffsetInfo baseOfs() {
    static_assert(std::is_base_of<Base, T>::value, "");
    int offset = (int)static_cast<Base*>((T *)0x01) - 0x01;
    OffsetInfo info = {offset, Base::typeInfo()};
    return info;
}

#define DEFINE_OBJ_INFO(this_type, ...) \
    ::GC::TypeInfo * createTypeInfo() const { \
        ::GC::OffsetInfo offsets[] = {__VA_ARGS__}; \
        ::GC::TypeInfoArgs args = {sizeof(this_type), (::GC::Finalizer)&this_type::finalizer, countOf(offsets), offsets}; \
        return ::GC::TypeInfo::alloc(args); \
    }\
    \
    static ::GC::TypeInfo * typeInfo() {\
        static ::GC::TypeInfo *nodeTypeInfo = ((this_type *)nullptr)->createTypeInfo();\
        return nodeTypeInfo;\
    }\
    \
    static void finalizer(void *self) { \
        printf(#this_type " finalizer at 0x%x\n", self); \
        ((this_type *)self)->~this_type();\
    }

struct Node
{
    int value;
    Node *next;

    DEFINE_OBJ_INFO(Node,
        fld(next));

    static void NodeFIn(Node *node) {
        printf("Node 0x%x finalizer\n", node);
    }
};


TEST(TypeInfo, pointer_type_info)
{
    const TypeInfo *tinfo = TypeInfo::pointerInfo();
    EXPECT_TRUE(tinfo != nullptr);
    EXPECT_TRUE(tinfo->offsetCount() == 1);
    EXPECT_TRUE(tinfo->size() == sizeof(void*));
    EXPECT_TRUE(tinfo->finalizer() == nullptr);
}

TEST(TypeInfo, test)
{
    {
        TypeInfoArgs args;
        args.finalizer = (Finalizer)0x1;
        args.offsetCount = 2;
        OffsetInfo offsets[2] = {0, TypeInfo::pointerInfo(), 2, TypeInfo::pointerInfo()};
        args.offsets = offsets;
        std::auto_ptr<TypeInfo> info( TypeInfo::alloc(args) );

        EXPECT_TRUE(info->finalizer() == args.finalizer);
        EXPECT_TRUE(info->size() == args.size);
        EXPECT_TRUE(info->offsetCount() == args.offsetCount);
    }

    Node node;
    node.next = &node;
    ClientObject *nodePtr = Node::typeInfo()->pointerAtIndexOfStruct((ClientStruct *)&node, 0);
    EXPECT_TRUE((ClientObject *)node.next == nodePtr);
}

TEST(TypeInfo, macro_test_with_nested_types)
{
    struct MyStruct {
        Node next;
        Node prev;
        void *pointer;

        DEFINE_OBJ_INFO(MyStruct,
            fld(next),
            fld(prev),
            fld(pointer));
    };

    TypeInfo *info = MyStruct::typeInfo();

    EXPECT_TRUE(info->offsetCount() == 3);
    EXPECT_TRUE(info->size() == sizeof(MyStruct));
    EXPECT_TRUE(info->offsetAt(0) == 4);
    EXPECT_TRUE(info->offsetAt(1) == 12);
}

TEST(TypeInfo, macro_test_with_nested_types_and_derived)
{
    struct MyStruct : Node {
        int value;
        Node prev;
        void *pointer;

        DEFINE_OBJ_INFO(MyStruct,
            baseOfs<MyStruct, Node>(),
            fld(prev),
            fld(pointer));
    };

    TypeInfo *info = MyStruct::typeInfo();

    EXPECT_TRUE(info->offsetCount() == 3);
    EXPECT_TRUE(info->size() == sizeof(MyStruct));
    EXPECT_TRUE(info->offsetAt(0) == 4);
    //EXPECT_TRUE(info->offsetAt(1) == 12);
}


TEST(Entity, from_memory_cast)
{
    Entity *entity = Entity::alloc(Node::typeInfo(), 1);

    EXPECT_TRUE(entity == Entity::fromMemory(entity->memory()));

    Entity::dealloc(entity);
}

TEST(Environment, gc_simple_references)
{
    Environment environment;

    Node *node0 = environment.allocEntityRoot<Node>();
    Node *node1 = environment.allocEntity<Node>();
    Node *node2 = environment.allocEntity<Node>();
    Node *node3 = environment.allocEntity<Node>();

    node0->next = node1;

    EXPECT_TRUE(environment.entitiesCount() == 4);

    environment.collectGarbage();

    EXPECT_TRUE(environment.entitiesCount() == 2);
}


TEST(Environment, gc_circular_references)
{
    Environment environment;

    Node *node0 = environment.allocEntityRoot<Node>();
    Node *node1 = environment.allocEntity<Node>();
    Node *node2 = environment.allocEntity<Node>();
    Node *node3 = environment.allocEntity<Node>();

    node0->next = node1;
    node1->next = node0;

    // circular. reference
    node2->next = node3;
    node3->next = node2;

    EXPECT_TRUE(environment.entitiesCount() == 4);

    environment.collectGarbage();

    EXPECT_TRUE(environment.entitiesCount() == 2);
}
