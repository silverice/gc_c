#include "gtest.h"

int main(int argc, char** argv)
{
    testing::runTests(meta<testing::TestInfo>::getListConst());
    return 0;
}
